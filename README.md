# [Lynda.com / Alex Zanfir] Test-Driven Development with Node.js [2015, ENG]


**Technologies:** Mocha, Should, Supertest

<br/>

008 Making sure you fail first

    # npm install -g mocha

    $ pwd
    /projects

    $ npm init
    $ npm install --save co-mocha
    $ npm install --save should
    $ npm install --save co-fs

    $ mocha --harmony

<br/>

    ReferenceError: data is not defined


<br/>

009 Getting your test to pass

    $ mocha --harmony

<br/>

010 Introducing the SuperTest framework

    $ npm install --save co-supertest
    $ npm install --save koa
    $ npm install --save koa-router


<br/>

011 Refactoring your code


<br/>

012 Trying the final product

    $ node --harmony user-web.js


http://localhost:3000/user


<br/>
<br/>

    $ npm list -g --depth=0
    /usr/local/lib
    ├── mocha@3.0.2
    └── npm@2.15.9


<br/>

    $ npm list --depth=0
    projects@1.0.0 /projects
    ├── co-fs@1.2.0
    ├── co-mocha@1.1.3
    ├── co-supertest@0.0.10
    ├── koa@1.2.4
    ├── koa-router@5.4.0
    ├── mocha@3.0.2
    ├── should@11.1.0
    └── supertest@1.2.0

___

**Marley**

<a href="https://jsdev.org">jsdev.org</a>

email:  
![Marley](http://img.fotografii.org/a3333333mail.gif "Marley")
